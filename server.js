const express = require("express");
const app = express();
const http = require("http").createServer(app);
const port = 1911;
const io = require("socket.io")(http);

app.use(express.static("public"));

app.get("/", (req, res) => res.sendFile("index.html"));
app.get("/", (req, res) => res.sendFile("style.css"));
app.get("/", (req, res) => res.sendFile("app.js"));

/* Début instructions serveur de Socket.io */

/* Fin instructions serveur de Socket.io */

http.listen(port, () => console.log(`Example app listening on port ${port}!`));